<div class="modal fade" id="settings-modal" name="settings-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Settings</h4>
            </div>
            <form id="set_settings" method="get" action="">
                <div class="modal-body">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"><span><i class="fa fa-weixin"></i></span> Rayz</a>
                                </h4>
                            </div>
                                <div id="collapseOne" class="panel-collapse collapse in">
                                    <div class="panel-body">
                                        <table class="table">
                                            <tr>
                                                <td>
                                                    <p>Distance metric</p>
                                                    <select name="distanceMetric" class="selectpicker">
                                                        <option>Kilometers</option>
                                                        <option>Miles</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Maximum rayz sending distance</p>
                                                    <select name="maxDistance" class="selectpicker">
                                                        <option>Unlimited</option>
                                                        <option>0.5 km</option>
                                                        <option>5 km</option>
                                                        <option>50 km</option>
                                                        <option>500 km</option>
                                                        <option>5000 km</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <p>Auto-star rayz i reply to</p>
                                                    <input name="autoStarReplies" checked data-toggle="toggle" type="checkbox">
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo"><span class="glyphicon glyphicon-th">
                                    </span> General</a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <p>Live tile</p>
                                                <input name="liveTile" checked data-toggle="toggle" type="checkbox">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>                                            
                                                <p>Notification sound</p>
                                                <input name="notificationSound" checked data-toggle="toggle" type="checkbox">
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseThree"><span ><i class="fa fa-map-marker"></i></span> Location</a>
                                </h4>
                            </div>
                            <div id="collapseThree" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <table class="table">
                                        <tr>
                                            <td>
                                                <p>Location</p>
                                                <input name="location" checked data-toggle="toggle" type="checkbox">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="question">Why should you keep location on?</p>
                                                <p>Location allows you to send and receive accurate location based messages. If it is turned off, you will receive rayz messages based on your last known location and you may miss important rayz messages around you!</p>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <p class="question">Does location use GPS and drains my phone's or laptop's battery?</p>
                                                <p>No. Location uses a cell-tower based location service instead of GPS positioning. As for your laptop, it calulates the current location over the internet. This does notcause excessive battery consumption.</p>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                 </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>  
                    <button type="submit" class="btn btn-default">Save settings</button>  
                </div>
            </form>
        </div>
    </div>
</div>

<script>
    $('.selectpicker').selectpicker();
    
    (function($) {
        $.fn.serializeFormJSON = function() {
            var o = {};
            var a = this.serializeArray();
            $.each(a, function() {
                if (o[this.name]) {
                    if (!o[this.name].push) {
                        o[this.name] = [o[this.name]];
                    }
                    o[this.name].push(this.value || '');
                } else {
                    o[this.name] = this.value || '';
                }
           });
           return o;
        };
    })(jQuery);
    
    $('#set_settings').submit(function(e) {
        $('#settings-modal').modal('toggle');
        var $form = $(this);
        var data = $form.serializeFormJSON();
        
        USER_SETTINGS=data;
        localStorage.setItem('USER_SETTINGS', JSON.stringify(USER_SETTINGS));
        
        e.preventDefault();
    });
</script>