<div class="modal fade" id="my-rayz-modal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">My Rayz:</h4>
            </div>
            <div class="modal-body">
                    <ul class="list-group" id="my-rayz-list">

                    </ul>
             </div>
            <div class="modal-footer">

                <li id="my-rayz-page-number" class="list-group-item" value="1" style="display:inline; float:left; "><div>Page: 1</div></li>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" onclick="myRayzPreviousPage()" class="btn btn-default"><i class="fa fa-arrow-circle-left"></i></button>
                <button type="button" onclick="myRayzNextPage()" class="btn btn-default"><i class="fa fa-arrow-circle-right"></i></button>
            </div>
        </div>
    </div>
</div>