<nav class="navbar navbar-default navbar-fixed-top top-menu">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="http://www.rayzit.com/#home"><img height="40px"  src="_/img/logo.jpg" alt="Rayzit"></a>
    </div>
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        
      <ul class="nav navbar-nav navbar-right">
        <li><a onclick="showModal();"  href="#">New rayz <span class="sr-only"></span></a></li>
        <li><a onclick="showMyStarModal();" href="#">Starred Rayz</a></li>
        <!--li><a onclick="showMyRayz();" href="#">My Rayz</a></li-->
        <li><a onclick="showSettings();" href="#">Settings</a></li>
      </ul>
        
    </div> 
  </div>
</nav>
<script>
    function showMyRayz(){
        var page = $("#my-rayz-page-number").val();
        getMyRayzForNav(page);
        $("#my-rayz-modal").modal('toggle');
    }

    function myRayzNextPage(){
        var page = $("#my-rayz-page-number").val();
        page++;
        getMyRayzForNav(page);
    }
    function myRayzPreviousPage(){
        var page = $("#my-rayz-page-number").val();
        page--;
        if (page>0) {
            getMyRayzForNav(page);
        }
    }

    function showSettings(){
        $('#settings-modal').modal('toggle');
    }
    
    function showMyStarModal(){
        getStarRayz(1);
        $("#star-rayz-modal").modal('toggle');
    }

    function starRayzPreviousPage(){
        var page = $("star-rayz-page-number").val();
        page--;
        if (page>0){
            getStarRayz(page);
        }
    }
    function starRayzNextPage(){
        var page = $("star-rayz-page-number").val();
        page++;
        getStarRayz(page);
    }

    function showModal(){
        $('#new_rayz_modal').modal('toggle');
    }
</script>