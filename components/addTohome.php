<style type="text/css">
            .ath-samsungAndroid {
                bottom: 1.8em;
                left: 50%;
                margin-left: -9em;
            }
            
            .ath-stockAndroid {
                right: 1.5em;
                top: 1.8em;
            }
            
            .ath-samsungAndroid:after {
                content: '';
                background: #eee;
                background: -webkit-linear-gradient(135deg, rgba(238,238,238,0) 0%,rgba(238,238,238,0) 50%,rgba(238,238,238,1) 50%,rgba(238,238,238,1) 100%);
                position: absolute;
                width: 2em;
                height: 2em;
                bottom: -1.5em;
                left: 0;
            }
            
            .ath-stockAndroid:after {
                content: '';
                background: #eee;
                background: -webkit-linear-gradient(-45deg, rgba(238,238,238,0) 0%,rgba(238,238,238,0) 50%,rgba(238,238,238,1) 50%,rgba(238,238,238,1) 100%);
                position: absolute;
                width: 2em;
                height: 2em;
                top: -1.5em;
                right: 0;
            }
            
            .ath-container.ath-stockAndroid:before {
                float: left;
                margin: -0.7em 0.5em 0 -0.6em;
            }
            
            .ath-container.ath-stockAndroid.ath-icon:before {
                position: absolute;
                right: auto;
                left: 0;
                margin: 0;
                float: none;
            }

            .ath-stockAndroid .ath-action-icon,.ath-samsungAndroid .ath-action-icon {
                width: 1.2em;
                height: 1.2em;
            }
        </style>
        
        <script>
            var _ath = addToHomescreen;
            var athMessages = {
                samsungAndroid: 'Custom message for Samsung Android. Click the menu hardware button and blah blah blah. This is an icon if needed: %icon',
                stockAndroid: 'Custom message for stock Android. Click the menu hardware button and blah blah blah. This is an icon if needed: %icon'
            };
            
            var _ua = window.navigator.userAgent;
            _ath.isAndroidBrowser = _ua.indexOf('Android') > -1 && !(/Chrome\/[.0-9]*/).test(_ua);
            _ath.isCompatible = _ath.isCompatible || _ath.isAndroidBrowser;
            if ( _ath.OS == 'unsupported' && _ath.isAndroidBrowser ) {
                _ath.OS = (/ (GT-I9|GT-P7|SM-T2|GT-P5|GT-P3|SCH-I8)/).test(_ua) ? 'samsungAndroid' : 'stockAndroid';
            }
            _ath({
                message: "Wanna feel like the real thing? Add rayzit to your homescreen.",
                skipFirstVisit: false,
                displayPace: false,
                icon:true
            });
         </script>
        