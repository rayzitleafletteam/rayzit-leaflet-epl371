<div class="modal fade" id="replies_modal">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            <h4 class="modal-title">Replies to:</h4>
        </div>
        <div class="modal-body">
            <div class="panel panel-default">
                <div class="panel-heading"></div>
                <div class="list-wrapper">
                <ul class="replies-list list-group"></ul>
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <?php include 'components/power_bar.php'?>
            <form class="form-horizontal">
                <div class="form-group">
                    <div class="col-md-12 col-sm-12 col-xs-12">
                        <input type="text" class="form-control" maxlength="480" id="your_reply" placeholder="Rayz your reply...">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-sm-offset-2 col-xs-offset-2 col-md-offset-2 col-md-10 col-sm-10 col-xs-10">
                        <button type="submit" id="btnreply" class="btn btn-default">Reply</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
