<div id="new_rayz_modal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">New rayz</h4>
            </div>
            <form id="new_rayz_form" name="new_rayz_form" class="form-horizontal" >
                <fieldset>
                    <div class="modal-body">
                        <div class="form-group">
                          <div class="col-md-12">                     
                            <textarea class="form-control" required id="rayz_message" name="rayz_message" placeholder="Rayz your message..."></textarea>
                          </div>
                        </div>                
                        <div class="form-group">
                          <div class="col-md-12">
                            <input id="file_input" name="file_input" class="input-file" type="file">
                          </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <?php include 'components/power_bar.php'?>
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Send</button>
                    </div>
                </fieldset>
            </form>   
        </div>
    </div>
</div>

<script>
    $("#file_input").fileinput({showUpload: false});
    $("form[name='new_rayz_form']").submit(function(event){
        $('#new_rayz_modal').modal('toggle');
        event.preventDefault();
    });
    
    $("form[name='new_rayz_form']").submit(function(e) {
        var formData = new FormData($(this)[0]);
        var message = $("#rayz_message").val();
        var distance = USER_SETTINGS.maxDistance.slice(0, -3);
        
        var postMessage = {
            "userId":USER_ID,
            "rayzMessage":message,
            "latitude":USER_LNG,
            "longitude":USER_LAT,
            "accuracy":1000,
            "maxDistance":distance 
        };
        
        console.log(postMessage);
        
        $.ajax({
            type: "POST",
            url: CREATE_URL,
            data: postMessage,
            success: function(returnData){
                var color="green";
                if(returnData.power >70){
                    color="green";
                }else if (returnData.power<=70 && returnData.power > 50){
                    color ="#CC0000";
                }else{
                    color="#FF9900";
                }
                
                $("#bar").css('background-color',color).css("width",returnData.power+"%");
                $("#bar").attr("aria-valuenow",returnData.power);
            }
        });
        
        e.preventDefault();
    });
            
    $('#new_rayz_modal').on('shown.bs.modal', function () {
        var rayzForm = document.getElementById("new_rayz_form");
        rayzForm.reset();
        $('#rayz_message').focus();
    });
    
</script>