/*Create a new marker for the specified coordiates.*/
var marker = L.marker([34.79576, 33.22266]);

/*Create a new circle with center as the coordinates specified and radius also specified.*/
var circle = L.circle([51.508, -0.11], 500, {
    color: 'red',
    fillColor: '#f03',
    fillOpacity: 0.5
});

/*Create a new polygon with edges the coordinates specified.*/
var polygon = L.polygon([
    [51.509, -0.08],
    [51.503, -0.06],
    [51.51, -0.047]
]);

/*Add the above elements on the map.*/
tile_layer.addTo(map);
marker.addTo(map);
circle.addTo(map);
polygon.addTo(map);

/*Create some popup windows for the elements above and set one open.*/
marker.bindPopup("<b>Hello world!</b><br>I am a popup.").openPopup();
circle.bindPopup("I am a circle.");
polygon.bindPopup("I am a polygon.");

/*Create a standalone popup at the given coordinates, and open it.*/
var popup = L.popup().setLatLng([34.79576, 33.22266]).setContent("I am a standalone popup.").openOn(map);