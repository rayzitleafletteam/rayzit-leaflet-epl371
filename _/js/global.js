//var USER_ID = prompt('Give a username');
var USER_ID = 'epl371';
var APP_ID = 'EPL371_AVC3PAFGO054FLP3AGEL0L';
var MY_RAYZ_PAGE =1;
var LIVEFEED_URL = 'http://dev.rayzit.com:9000/user/'+USER_ID+'/livefeed';
//var LIVEFEED_URL = 'http://194.42.17.174:9000/user/'+USER_ID+'/livefeed';
var RECENTFEED_URL = 'http://dev.rayzit.com:9000/user/'+USER_ID+'/livefeedrecent';
//var RECENTFEED_URL = 'http://194.42.17.174:9000/user/'+USER_ID+'/livefeedrecent';
var MYRAYZ_URL = 'http://dev.rayzit.com:9000/user/'+USER_ID+'/myrayz/';
//var MYRAYZ_URL = 'http://194.42.17.174:9000/user/'+USER_ID+'/myrayz/'
var POWER_URL = 'http://dev.rayzit.com:9000/user/'+USER_ID+'/power';
//var POWER_URL = 'http://194.42.17.174:9000/user/'+USER_ID+'/power';
var UPDATE_LOCATION_URL = 'http://dev.rayzit.com:9000/user/update';
//var UPDATE_LOCATION_URL = 'http://194.42.17.174:9000/user/update';
var CREATE_URL = 'http://dev.rayzit.com:9000/rayz/create';
//var CREATE_URL = 'http://194.42.17.174:9000/rayz/create';
var STAR_RAYZ_URL = 'http://dev.rayzit.com:9000/user/'+USER_ID+'/starred/';
//var STAR_RAYZ_URL = 'http://194.42.17.174:9000/user/'+USER_ID+'/starred/';
var REPLIES = 'http://dev.rayzit.com:9000/rayz/replies';
//var REPLIES = 'http://194.42.17.174:9000/rayz/replies';
var REPLY = 'http://dev.rayzit.com:9000/rayz/reply';
//var REPLY = 'http://194.42.17.174:9000/rayz/reply';
var DOMAIN = 'http://dev.rayzit.com:9000';
var IP = 'http://194.42.17.174:9000';
var POWERUP = 'http://dev.rayzit.com:9000/rayz/reply/powerup';
//var POWERUP = 'http://194.42.17.174:9000/rayz/reply/powerup';
var POWERDOWN = 'http://dev.rayzit.com:9000/rayz/reply/powerdown';
//var POWERDOWN = 'http://194.42.17.174:9000/rayz/reply/powerdown';
var STAR_URL = 'http://dev.rayzit.com:9000/rayz/star';
//var STAR_URL = 'http://194.42.17.174:9000/rayz/star';
var UNSTAR_URL = 'http://dev.rayzit.com:9000/rayz/star/delete';
//var UNSTAR_URL = 'http://194.42.17.174:9000/rayz/star/delete';
var RERAYZ_URL = 'http://dev.rayzit.com:9000/rayz/rerayz';
//var RERAYZ_URL = 'http://194.42.17.174:9000/rayz/rerayz';
var USER_LAT = null;
var USER_LNG = null;

var targetLayer,targetMarker
function display_time(msgtime){
    var time = new Date(msgtime);
    time = new Date(time.getUTCFullYear(), time.getUTCMonth(), time.getUTCDate(),  time.getUTCHours(), time.getUTCMinutes(), time.getUTCSeconds());
    if(time.getDate()==new Date().getDate()
        && time.getMonth()==new Date().getMonth()
        && time.getFullYear()==new Date().getFullYear()) {
        time = Math.abs(new Date().getTime() - time);
        time = Math.ceil(time / 1000);
        if(time<60){
            if(time<2){
                time="a second ago";
            }
            else{
                time=time+" seconds ago";
            }

        }else if(time>=60){
            time=new Date(msgtime);
            time = new Date(time.getUTCFullYear(), time.getUTCMonth(), time.getUTCDate(),  time.getUTCHours(), time.getUTCMinutes(), time.getUTCSeconds());
            time = Math.abs(new Date() - time);
            time = Math.ceil(time / (1000*60));
            if(time<60){
                if(time<2){
                    time="a minute ago";
                }
                else{
                    time=time+" minutes ago";
                }

            }else if(time>=60){
                time=new Date(msgtime);
                time = new Date(time.getUTCFullYear(), time.getUTCMonth(), time.getUTCDate(),  time.getUTCHours(), time.getUTCMinutes(), time.getUTCSeconds());
                time = Math.abs(new Date() - time);
                time = Math.ceil(time / (1000*60*60));
                if(time<24) {
                    if (time < 2) {
                        time = "an hour ago";
                    }
                    else {
                        time = time + " hours ago";
                    }
                }
            }
        }
    }else if( time.getMonth()==new Date().getMonth()
        && time.getFullYear()==new Date().getFullYear()){
        time = Math.abs(new Date() - time);
        time = Math.ceil(time / (1000*3600*24));
        if(time<2){
            time="a day ago";
        }
        else{
            if(time<7){
                time=time+" days ago";
            }else{
                time=new Date(msgtime);
                time = new Date(time.getUTCFullYear(), time.getUTCMonth(), time.getUTCDate(),  time.getUTCHours(), time.getUTCMinutes(), time.getUTCSeconds());
                time = Math.abs(new Date() - time);
                time = Math.ceil(time / (1000*3600*24*7));
                if(time<2){
                    time="a week ago";
                }else{
                    time= time+" weeks ago";
                }
            }

        }
    }else if(time.getFullYear()==new Date().getFullYear()){
        time = time.getDate() + " " + time.toUTCString().split(' ')[2]+" "+time.toUTCString().split(' ')[4];
    }else{
        time = time.getDate()+ " " + time.toUTCString().split(' ')[2]+" "+time.getFullYear()+" "+time.toUTCString().split(' ')[4];
    }
    return time;
}