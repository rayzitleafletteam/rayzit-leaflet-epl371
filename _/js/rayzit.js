function updateLocation(lat,lng){
        var updateLocation_info = {
            "userId":USER_ID,
            "appId":APP_ID,
            "latitude":lat,
            "longitude":lng,
            "accuracy":1000
        };

        $.ajax({
            type: "POST",
            url: UPDATE_LOCATION_URL,
            data: updateLocation_info
        });

    USER_LAT=lat;
    USER_LNG=lng;
}

function sendRayz(){
    var message = $("#rayz_message").val();
    var distance;
    
    switch(USER_SETTINGS.maxDistance) {
        case "0.5 km":
            distance=0.5;
            break;
        case "5 km":
            distance=5;
            break;
        case "50 km":
            distance=50;
            break;
        case "500 km":
            distance=500;
            break;
        case "5000 km":
            distance=5000;
            break;
        default:
            distance=0;
    } 
    var postMessage = {"userId":USER_ID,"rayzMessage":message, "latitude":USER_LAT, "longitude":USER_LNG, "accuracy":1000,"maxDistance":distance };

    console.log(postMessage);

    $.ajax({
        type: "POST",
        url: CREATE_URL,
        data: postMessage,
        success: function(returnData){
            console.log(returnData.power);
            var color="green";
            if(returnData.power >70){
                color="green";
            }else if (returnData.power<=70 && returnData.power > 50){
                color ="#CC0000";
            }else{
                color="#FF9900";
            }
            $(".power-bar").css('background-color',color).css("width",returnData.power+"%");
            $(".power-bar").attr("aria-valuenow",returnData.power);
            console.log(returnData);
        }
    });
}

function setPower(){
    var power = $(".power-bar").attr("aria-valuenow");
    var color="green";
    if(power>75){
        color="green";
    }else if (power<=75 && power > 50){
        color ="#FF9900";
    }else{
        color="#CC0000";
    }
    $(".power-bar").css("width",power +"%").css("background-color",color);
    $(".power-bar").attr("aria-valuenow",power);
}

function getPower(){
    $.get(POWER_URL,function(data){
        $(".power-bar").attr("aria-valuenow",data.power);
        console.log(data.power);
        setPower();
        setInterval("setPower()",100000);
    });
}

function getLiveFeed(){
    var time;
    $.get(LIVEFEED_URL,function(data,status){
        jQuery.each( data.liveFeed, function( index, value ) {
            if(value.userId!="") {
                time = display_time(value.timestamp);
                $('ul[name="livefeed-list"]').append("<li href='#' class='list-group-item' id='rayz_" + index + "'><a style='text-decoration: none' id='ar_"+index+"' href='#'><p class='reply'>" + value.rayz_message + "</p></a></li>");
                $('ul[name="livefeed-list"] #rayz_' + index+' a').prepend("<div class='row'><div class='col-lg-6 col-md-6 col-xs-6'><p class='time'>a moment ago</p></div><div class='col-lg-push-4 col-md-push-4 col-xs-push-3 col-lg-6 col-md-6 col-xs-6'><i style='color:grey' class='fa fa-circle'></i></div></div>");
                $('ul[name="livefeed-list"] #rayz_' + index+' a').append("<p style='display:inline-block' class='starred'>" + value.follow + " starred, </p><p style='display:inline-block'  class='rerayz'> " + value.rerayz + " rerayz</p>");
                $('ul[name="livefeed-list"] #rayz_' + index).append("<div class='restar-wrapper row'>" +
                "<div class='col-lg-6 col-md-6 col-xs-6'><a href='#' id='star_" + index + "'>" +
                "<i class='fa fa-star-o'><span> star</span></i></a></div> " +
                "<div class='col-lg-push-2 col-md-push-1 col-xs-push-1 col-lg-6 col-md-6 col-xs-6'><a style='display:inline-block' href='#' id='rerayz_" + index + "'>" +
                "<i class='fa fa-refresh'><span> rerayz</span></i></a></div>" +
                "</div>");
                placeMarker(value, 'orange');
            }
        });
        $('ul[name="livefeed-list"] .list-group-item > a').click(function(){
            var replydet=$(this).attr('id').split('_')[1];
            showReplies(data.liveFeed[replydet]);
        });

        $('ul[name="livefeed-list"] .restar-wrapper a:first-child').click(function(){
            var stardet=$(this).attr('id').split('_')[1];
            var starjson='{"userId":\"'+USER_ID+'\","rayzId":\"'+data.liveFeed[stardet].rayzId+'\"}'
            starjson=jQuery.parseJSON(starjson);
            if($('ul[name="livefeed-list"] #star_'+stardet+' span').html()==" star")
                $.post(STAR_URL,starjson,function(data){
                    if(data.status=='success'){
                        $('ul[name="livefeed-list"] #star_'+stardet+' span').html(' unstar');
                        $('ul[name="livefeed-list"] #rayz_' + stardet+' .starred').html(data.follow+' starred,');
                    }
                });
            else
                $.post(UNSTAR_URL,starjson,function(data){
                    if(data.status=='success'){
                        $('ul[name="livefeed-list"] #star_'+stardet+' span').html(' star');
                        $('ul[name="livefeed-list"] #rayz_' + stardet+' .starred').html(data.follow+' starred,');
                    }
                });
        });
        $('ul[name="livefeed-list"] .restar-wrapper div:nth-child(2) a').click(function(){
            var stardet=$(this).attr('id').split('_')[1];
            var starjson='{"userId":\"'+USER_ID+'\","rayzId":\"'+data.liveFeed[stardet].rayzId+'\"' +
                ',"latitude":\"'+USER_LAT+'\","longtitude":\"'+USER_LNG+'\","accuracy":"0","maxDistance":"0"}'
            starjson=jQuery.parseJSON(starjson);

            var jqXHR =
                $.ajax(
                    {
                        type: "POST",
                        url: RERAYZ_URL,
                        async: false,
                        data: {
                            userId:starjson.userId,
                            rayzId:starjson.rayzId,
                            latitude:starjson.latitude,
                            longitude:starjson.longtitude,
                            accuracy:starjson.accuracy,
                            maxDistance:starjson.maxDistance
                        }
                    }
                );
            jqXHR.done( function (data) {
                if(data.status){
                    $('ul[name="livefeed-list"] li#rayz_' + stardet+' a#ar_'+stardet+' .rerayz').html(data.rerayz+' rerayz');
                }
            });

        });

    });
}

function getStarRayz(pageNumber){
    var time;
    $.get(STAR_RAYZ_URL +pageNumber,function(data,status){
        if (data.counter > 0) {
            $('#star-rayz-modal #star-rayz-list').html("");
            $('#star-rayz-modal .modal-body').slimScroll({
                height: '50vh'
            });
            jQuery.each(data.rayzFeed, function (index, value) {
                if(value.userId!="") {
                    time = display_time(value.timestamp);
                    $('#star-rayz-modal #star-rayz-list').append("<li class='list-group-item' id='rayz_" + index + "'><a style='text-decoration: none' id='ar_"+index+"' href='#'><p class='reply'>" + value.rayz_message + "</p></a></li>");
                    $('#star-rayz-modal #star-rayz-list #rayz_' + index+' a').prepend("<div class='row'><div class='col-lg-5 col-md-5 col-xs-6'><p class='time'>a moment ago</p></div><div class='col-lg-push-6 col-md-push-6 col-xs-push-3 col-lg-6 col-md-6 col-xs-6'><i style='color:#ffd730' class='fa fa-circle'></i></div></div>");
                    $('#star-rayz-modal #star-rayz-list #rayz_' + index+' a').append("<p style='display:inline-block' class='starred'>" + value.follow + " starred, </p><p style='display:inline-block'  class='rerayz'> " + value.rerayz + " rerayz</p>");
                    $('#star-rayz-modal #star-rayz-list #rayz_' + index).append("<div class='restar-wrapper row'>" +
                    "<div class='col-lg-6 col-md-6 col-xs-6'><a href='#' id='star_" + index + "'>" +
                    "<i class='fa fa-star-o'><span> unstar</span></i></a></div> " +
                    "<div class='col-lg-push-2 col-md-push-2 col-xs-push-1 col-lg-6 col-md-6 col-xs-6'><a style='display:inline-block' href='#' id='rerayz_" + index + "'>" +
                    "<i class='fa fa-refresh'><span> rerayz</span></i></a></div>" +
                    "</div>");
                    $('#star-rayz-modal #star-rayz-list .restar-wrapper').css('border-color','#ffd730');
                }
            });
            $("#star-page-number").val(pageNumber).text("Page:"+pageNumber);
            $('#star-rayz-modal #star-rayz-list .list-group-item>a').click(function(){
                var replydet=$(this).attr('id').split('_')[1];
                showReplies(data.rayzFeed[replydet]);
            });

            $('ul#star-rayz-list li .row .restar-wrapper a:nth-child(2)').click(function(){
                var stardet=$(this).attr('id').split('_')[1];
                var starjson='{"userId":\"'+USER_ID+'\","rayzId":\"'+data.rayzFeed[stardet].rayzId+'\"' +
                    ',"latitude":\"'+USER_LAT+'\","longitude":\"'+USER_LNG+'\","accuracy":"0","maxDistance":"0"}'
                starjson=jQuery.parseJSON(starjson);

                var jqXHR =
                    $.ajax(
                        {
                            type: "POST",
                            url: RERAYZ_URL,
                            async: false,
                            data: {
                                userId:starjson.userId,
                                rayzId:starjson.rayzId,
                                latitude:starjson.latitude,
                                longitude:starjson.longitude,
                                accuracy:starjson.accuracy,
                                maxDistance:starjson.maxDistance
                            }
                        }
                    );
                jqXHR.done( function (data) {
                    if(data.status){
                        $('ul#star-rayz-list li#rayz_' + stardet+' a#ar_'+stardet+' .rerayz').html(data.rerayz+' rerayz');
                    }
                });

            });

            
            $('#star-rayz-modal #star-rayz-list .list-group-item .restar-wrapper a:first-child').click(function(){
                var stardet=$(this).attr('id').split('_')[1];
                var starjson='{"userId":\"'+USER_ID+'\","rayzId":\"'+data.rayzFeed[stardet].rayzId+'\"}'
                starjson=jQuery.parseJSON(starjson);

                $.post(UNSTAR_URL,starjson,function(data){
                    if(data.status=='success'){
                        $('#star-rayz-modal #star-rayz-list li#rayz_' + stardet).remove();
                    }
                });
            });
        }
    });
}



function getMyRayzForNav(pageNumber){

    $.get(MYRAYZ_URL+pageNumber,function(data,status) {
        if (data.counter > 0) {
            $('#my-rayz-modal #my-rayz-list').html("");
            jQuery.each(data.myRayz, function (index, value) {
                $('#my-rayz-modal #my-rayz-list').append("<li class='list-group-item'>" + value.rayz_message + "</li>");
            });
            $("#my-rayz-page-number").val(pageNumber).text("Page:"+pageNumber);
        }

    });
}


function getRecentLiveFeed(){
    $.get(RECENTFEED_URL,function(data,status){
        if(data.counter==0){
            $('ul[name="recentfeed-list"]').append('<p style="color:#FFFFFF">N/A</p>');
        }
        else{
            jQuery.each( data.liveFeed, function( index, value ) {
                if(value.userId!="") {
                    time = display_time(value.timestamp);
                    $('ul[name="recentfeed-list"]').append("<li class='list-group-item' id='rayz_" + index + "'><a style='text-decoration: none' id='ar_"+index+"' href='#'><p class='reply'>" + value.rayz_message + "</p></a></li>");
                    $('ul[name="recentfeed-list"] #rayz_' + index+' a').prepend("<div class='row'><div class='col-lg-6 col-md-6 col-xs-6'><p class='time'>a moment ago</p></div><div class='col-lg-push-4 col-md-push-4 col-xs-push-3 col-lg-6 col-md-6 col-xs-6'><i style='color:grey' class='fa fa-circle'></i></div></div>");
                    $('ul[name="recentfeed-list"] #rayz_' + index+' a').append("<p style='display:inline-block' class='starred'>" + value.follow + " starred, </p><p style='display:inline-block'  class='rerayz'> " + value.rerayz + " rerayz</p>");
                    $('ul[name="recentfeed-list"] #rayz_' + index).append("<div class='restar-wrapper row'>" +
                    "<div class='col-lg-6 col-md-6 col-xs-6'><a href='#' id='star_" + index + "'>" +
                    "<i class='fa fa-star-o'><span> star</span></i></a></div> " +
                    "<div class='col-lg-push-2 col-md-push-1 col-xs-push-1 col-lg-6 col-md-6 col-xs-6'><a style='display:inline-block' href='#' id='rerayz_" + index + "'>" +
                    "<i class='fa fa-refresh'><span> rerayz</span></i></a></div>" +
                    "</div>");
                    placeMarker(value, 'orange');
                }
            });
            $('ul[name="recentfeed-list"] .list-group-item>a').click(function(){
                var replydet=$(this).attr('id').split('_')[1];
                showReplies(data.liveFeed[replydet]);
            });

            $('ul[name="recentfeed-list"] .restar-wrapper a:first-child').click(function(){
                var stardet=$(this).attr('id').split('_')[1];
                var starjson='{"userId":\"'+USER_ID+'\","rayzId":\"'+data.liveFeed[stardet].rayzId+'\"}'
                starjson=jQuery.parseJSON(starjson);

                if($('ul[name="recentfeed-list"] #star_'+stardet+' span').html()==" star")
                    $.post(STAR_URL,starjson,function(data){
                        if(data.status=='success'){
                            $('ul[name="recentfeed-list"] #star_'+stardet+' span').html(' unstar');
                            $('ul[name="recentfeed-list"] #rayz_' + stardet+' .starred').html(data.follow+' starred,');
                        }
                    });
                else
                    $.post(UNSTAR_URL,starjson,function(data){
                        if(data.status=='success'){
                            $('ul[name="recentfeed-list"] #star_'+stardet+' span').html(' star');
                            $('ul[name="recentfeed-list"] #rayz_' + stardet+' .starred').html(data.follow+' starred,');
                        }
                    });
            });
            
           $('ul[name="recentfeed-list"] .restar-wrapper div:nth-child(2) a').click(function(){
            var stardet=$(this).attr('id').split('_')[1];
            var starjson='{"userId":\"'+USER_ID+'\","rayzId":\"'+data.liveFeed[stardet].rayzId+'\"' +
                ',"latitude":\"'+USER_LAT+'\","longtitude":\"'+USER_LNG+'\","accuracy":"0","maxDistance":"0"}'
            starjson=jQuery.parseJSON(starjson);

            var jqXHR =
                $.ajax(
                    {
                        type: "POST",
                        url: RERAYZ_URL,
                        async: false,
                        data: {
                            userId:starjson.userId,
                            rayzId:starjson.rayzId,
                            latitude:starjson.latitude,
                            longitude:starjson.longtitude,
                            accuracy:starjson.accuracy,
                            maxDistance:starjson.maxDistance
                        }
                    }
                );
            jqXHR.done( function (data) {
                if(data.status){
                    $('ul[name="recentfeed-list"] li#rayz_' + stardet+' a#ar_'+stardet+' .rerayz').html(data.rerayz+' rerayz');
                }
            });

        });


        }

    });
    
}

function getMyRayz(){
    $.get(MYRAYZ_URL+MY_RAYZ_PAGE,function(data,status){
        jQuery.each( data.myRayz,function( index, value ) {
            placeMarker(value,'cadetblue');
        });
    });
}

function showReplies(rayzfeed){
    $('#replies_modal').css('z-index','2000');
    user="\""+rayzfeed.userId.substring(5)+"\"";
    rayz="\""+rayzfeed.rayzId+"\"";
    var json = '{ "userId": '+user+',"rayzId": '+rayz+'}';
    json=jQuery.parseJSON(json);
    var time;
    var distance;
    if(rayzfeed.maxDistance==0){
        distance=' unlimited';
    }else{
        distance=" "+rayzfeed.maxDistance+" km"
    }
    $.post(REPLIES
        ,json
        ,function(data){
            time = display_time(rayzfeed.timestamp);
            $('#replies_modal .panel-heading')
                .html("<div class='det row'>"+
                "<div class='col-md-6 col-sm-6 col-xs-7'>"+
                "<span class='time'>"+time+"</span></div>" +
                "<div class='col-md-6 col-sm-5 col-xs-5'>"+
                "<i class='fa fa-globe'>"+distance+"</i></div>" +
                "</div><br>"+
                "<p class='rayzmsg'>"+rayzfeed.rayz_message+"</p><br>" +
                "<div class='det row'>"+
                "<div class='col-md-5 col-sm-5 col-xs-5'>"+
                "<span class='reptotal'>"+data.replies.length+" total</span></div>"+
                "<div class='col-md-7 col-sm-7 col-xs-7'>"+
                "<span class='star'>"+rayzfeed.follow+" </span><i class='fa fa-star-o'></i>"+
                "<span class='rerayz'> "+rayzfeed.rerayz+" </span><i class='fa fa-refresh'></i></div>"+
                "</div>");
            if(!$.isArray(data.replies) ||  !data.replies.length){
                $('#replies_modal .modal-body .list-wrapper').prepend("<div class='panel-body'>" +
                "<p>hmmmm,</p>" +
                "<p>It seems like no one replied yet..</p>"+
                "</div>");
            }else {
                $('#replies_modal .modal-body .list-wrapper').slimScroll({
                    height: '50vh'
                });
                $.each(data.replies, function (key, msg) {
                    time = display_time(msg.timestamp);
                    $('#replies_modal .list-group').append("<li class='list-group-item' id='rayz_" + key + "'><p class='reply'>" + msg.rayz_reply_message + "</p></li>");
                    $('#replies_modal .list-group #rayz_' + key).prepend("<div class='row'><div class='col-lg-6 col-md-6 col-xs-6'><p class='time'>a moment ago</p></div><div class='col-lg-push-5 col-md-push-5 col-xs-push-4 col-lg-6 col-md-6 col-xs-6'><i style='color:grey' class='fa fa-circle'></i></div></div>");
                    $('#replies_modal .list-group #rayz_' + key).append("<p class='reply-power'>" + msg.upVotes + " power</p>");
                    $('#replies_modal .list-group #rayz_' + key).append("<div class='power-wrapper row'>" +
                    "<div class='col-lg-6 col-md-6 col-xs-6'><a href='#' class='p-down' id='p-down_" + key + "'>" +
                    "<i class='fa fa-arrow-circle-down'><span> power down</span><span id='repdownid" + key + "' style='display:none'>" + msg.rayzReplyId + "</span></i></a></div> " +
                    "<a style='display:inline-block' href='#' class='p-up' id='p-up_" + key + "'>" +
                    "<div class='col-lg-push-5 col-md-push-5 col-xs-push-4 col-lg-6 col-md-6 col-xs-6'></div><i class='fa fa-arrow-circle-up'><span> power up</span><span id='repupid" + key + "' style='display:none'>" + msg.rayzReplyId + "</span></i></a></div>" +
                    "</div>");
                    if (msg.rayzReplyId.split('_')[1] == USER_ID) {
                        $('#rayz_' + key + ' i.fa-arrow-circle-down,#rayz_' + key + ' i.fa-arrow-circle-up').css("color", "grey");
                        $('#rayz_' + key + ' .row div:first-child a').removeClass('p-down');
                        $('#rayz_' + key + ' .row div:last-child a').removeClass('p-up');
                        $('#rayz_' + key + ' .power-wrapper').css("border-top-color", "blue");
                    }
                });
            }

            $('a.p-up').click(function(){
                //power_down();
                var itemid=$(this).attr('id').split('_')[1];
                var powerelement=$('#rayz_'+itemid+' .reply-power');
                var reply="\""+$('a.p-down #repdownid'+itemid).html()+"\"";
                var jsonpup = '{ "userId": '+"\""+USER_ID+"\""+',"rayzReplyId": '+reply+'}';
                var power=powerelement.html().split(' ')[0];
                jsonpup=jQuery.parseJSON(jsonpup);
                $.post(POWERUP,
                    jsonpup,
                    function(data){
                        if(data.status=="success"){
                            power=data.upVotes;
                            powerelement.html(power+" power");
                        }else{
                            alert(data.message);
                            powerelement.html(power+" power");
                        }
                    });
            });
            $('a.p-down').click(function(){
                //power_down();
                var itemid=$(this).attr('id').split('_')[1];
                var powerelement=$('#rayz_'+itemid+' .reply-power');
                var reply="\""+$('a.p-down #repdownid'+itemid).html()+"\"";
                var jsonpdown = '{ "userId": '+"\""+USER_ID+"\""+',"rayzReplyId": '+reply+'}';
                var power=powerelement.html().split(' ')[0];
                jsonpdown=jQuery.parseJSON(jsonpdown);
                $.post(POWERDOWN,
                    jsonpdown,
                    function(data){
                        if(data.status=="success"){
                            power=data.upVotes;
                            powerelement.html(power+" power");
                        }else{
                            if(data.message=='Insufficient power.'){
                                alert(data.message);
                            }
                            powerelement.html(power+" power");

                        }
                    });

            });



        });

    $('#your_reply').keyup(function(){
        var len = $(this).val().length;
        if (len <= 480 && len>0) {
            //$('#replies_modal #char_len').html(len+'/480');
            $('#btnreply').removeAttr("disabled","disabled");
        } else {
            if(len==0){
                //$('#replies_modal #char_len').html(len+'/480');
            }

            $('#btnreply').attr("disabled","disabled");
        }
    });

    $('#btnreply').click(function(){
        var itemid=0;
        if($('#replies_modal .list-group li:last-child').val()!=undefined)
            itemid=$('#replies_modal .list-group li:last-child').attr('id').split('_')[1]+1;
        var jsonrep = '{ "userId": '+"\""+USER_ID+"\""
            +',"rayzId": '+rayz+
            ',"rayzReplyMessage": '+"\""+$('#your_reply').val()
            +"\""+',"attachments": {} }';
        var fd = new FormData();
        fd.append('json',jsonrep);
        var jqXHR =
            $.ajax(
                {
                    type: "POST",
                    url: REPLY,
                    async: false,
                    data: fd,
                    processData: false,
                    contentType: false
                }
            );
        jqXHR.done( function (data) {
            if(data.status=="error"){
                alert(data.message);
            }else {
                if($('#replies_modal .modal-body .panel-heading')!=undefined){
                    $('#replies_modal .modal-body .panel div.panel-body').remove();
                }

                $('#replies_modal .list-group').append("<li class='list-group-item' id='rayz_" + itemid + "'><p class='reply'>" + $('#your_reply').val()+"</p></li>");
                $('#replies_modal .list-group #rayz_' + itemid).prepend("<div class='row'><div class='col-lg-6 col-md-6 col-xs-6'><p class='time'>a moment ago</p></div><div class='col-lg-push-5 col-md-push-5 col-xs-push-4 col-lg-6 col-md-6 col-xs-6'><i style='color:grey' class='fa fa-circle'></i></div></div>");
                $('#replies_modal .list-group #rayz_' + itemid).append("<p class='reply-power'>0 power</p>");
                $('#replies_modal .list-group #rayz_' + itemid).append("<div class='row'>" +
                "<div class='power-wrapper col-lg-12 col-md-12 col-xs-12'><a href='#' >" +
                "<i style='color:#808080'class='fa fa-arrow-circle-down'><span> power down</span></i></a> " +
                "<a href='#'>" +
                "<i style='color:#808080' class='fa fa-arrow-circle-up'><span> power up</span></i></a></div>" +
                "</div>");
                $('#rayz_' + itemid + ' .power-wrapper').css("border-top-color", "blue");
                $('#your_reply').val("");
            }
            if(USER_SETTINGS.autoStarReplies=="on"){
                var starjson='{"userId":\"'+USER_ID+'\","rayzId": '+rayz+'}'
                starjson=jQuery.parseJSON(starjson);
                $.post(STAR_URL,starjson,function(data){
                    console.log(data);
                });
            }
        });
    });


    //$('#replies_modal .modal-footer .form-group').append('<p id="char_len">0/480</p>');
    $('#btnreply').attr("disabled","disabled");
    $('#replies_modal').modal('toggle');
    $('#replies_modal').on('hide.bs.modal',function(){
        $('#replies_modal .list-group').empty();
        $('#your_reply').val("");
        $('#replies_modal .modal-footer .form-group p').remove();
        $('#replies_modal .panel-body').remove();
        $('#replies_modal .panel-heading').empty();
    });
}

$( document ).ready(function(){
    $('#replies_modal .list-group').empty();
    $('#your_reply').val("");
    $('#replies_modal .modal-footer .form-group p').remove();
})

function refresh(){
    $('ul[name="livefeed-list"]').html("");
    $('ul[name="recentfeed-list"]').html("");
    getLiveFeed();
    getRecentLiveFeed();
}

