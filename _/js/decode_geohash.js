var numbits = 5 * 5;
var digits = [ '0', '1', '2', '3', '4', '5', '6', '7', '8',
        '9', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'j', 'k', 'm', 'n', 'p',
        'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' ];

var lookup = [];

var i = 0;
digits.forEach(function(entry) {
    lookup[entry]=i++;
});

function decodeMaster(geohash) {
    var buffer = "";
    geohash.split('').forEach(function(c){
        
        var i = parseInt(lookup[c]) + 32;
        buffer+=i.toString(2).substring(1);
//        alert(c+" = "+c.charCodeAt(0)+" : "+lookup[c]+" + 32 = "+i+" => "+i.toString(2).substring(1));
    });

   var lonset = [];
   var latset = [];

    for (var i = 0; i < 64; i++) {
        lonset[i]=false;
        latset[i]=false;
    }

    var j = 0;
    for (var i = 0; i < numbits * 2; i += 2) {
        var isSet = false;
        if (i < buffer.length){
            isSet = (buffer.charAt(i) == '1');
        }
        lonset[j++]=isSet;
    }

    j = 0;
    for (var i = 1; i < numbits * 2; i += 2) {
        var isSet = false;
        if (i < buffer.length){
            isSet = (buffer.charAt(i) == '1');
        }
        latset[j++]=isSet;
    }

    var lon = decode(lonset, -180, 180);
    var lat = decode(latset, -90, 90);

    return [lat,lon];
}

function decode(lonset,floor,ceiling) {
    var mid = 0;
    for (var i = 0; i < lonset.length; i++) {
        mid = (floor + ceiling) / 2;
        if (lonset[i])
            floor = mid;
        else
            ceiling = mid;
    }
    return mid;
}
