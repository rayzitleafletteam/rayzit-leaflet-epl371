function centerMap (e) {
  map.panTo(e.latlng);
}

function zoomIn (e) {
  map.zoomIn();
}

function zoomOut (e) {
  map.zoomOut();
}

function showRange(m,d){
    addRange(m.getLatLng().lng+","+m.getLatLng().lat,m.options.range);
}

function checkRayz (e) {
    if (map.hasLayer(targetLayer)){
        targetLayer.removeLayer(targetMarker);
        map.removeLayer(targetLayer);
    }
    map.panTo(e.latlng);
    targetLayer=L.markerClusterGroup({ chunkedLoading: true });
    
    $('ul[name="livefeed-list"]').html("");
    $('ul[name="recentfeed-list"]').html("");
    
    var markerStyle = L.AwesomeMarkers.icon({
        icon: 'screenshot ',
        prefix: 'glyphicon',
        markerColor: 'darkred'
    });

    targetMarker = L.marker([e.latlng.lat,e.latlng.lng],{icon:markerStyle});

    targetLayer.addLayer(targetMarker);
    map.addLayer(targetLayer);
    
    updateLocation(e.latlng.lat,e.latlng.lng);
    getLiveFeed();
    getRecentLiveFeed();
    
}

var map = L.map('map_canvas',{
            zoomControl:false,
            contextmenu: true,
            contextmenuWidth: 140,
            contextmenuItems: [{
              text: 'Focus here',
              callback: centerMap
            }, {
                text: 'Check rayz',
                callback: checkRayz
            }, '-', {
              text: 'Zoom in',
              icon: '_/img/zoom-in.png',
              callback: zoomIn
            }, {
              text: 'Zoom out',
              icon: '_/img/zoom-out.png',
              callback: zoomOut
            }]
}).setView([35.0907, 33.31879], 7);

var markers = L.markerClusterGroup({ chunkedLoading: true });
var tile_layer = L.tileLayer('https://{s}.tiles.mapbox.com/v4/{mapId}/{z}/{x}/{y}.png?access_token={token}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    mapId: 'rayzitmap.lfldn508',
    token: 'pk.eyJ1IjoicmF5eml0bWFwIiwiYSI6InEwcEtCclkifQ.5UKlZjpklSsn6-5GGSoEWA'
});

tile_layer.addTo(map);

function placeMarker(rayzmarker,color){

    var markerStyle = L.AwesomeMarkers.icon({
        icon: 'comment',
        prefix: 'glyphicon',
        markerColor: color
    });

    var coordinates= decodeMaster(rayzmarker.geohash);
    var placemarker = L.marker([coordinates[1],coordinates[0]],{
        icon:markerStyle,
        range:rayzmarker.maxDistance,
        contextmenu: true,
        contextmenuItems: [{
            range:rayzmarker.maxDistance,
            text: 'View replies',
            callback: function(){
                showReplies(rayzmarker);
            },
            index: 0
            },{
            text: 'View range',
            callback: function(){
                showRange(placemarker);
            },
            index: 0
            },{
            separator: true,
            index: 1
        }]
    });

    var time=display_time(rayzmarker.timestamp);
    placemarker.on('click',function(){
        var jsonpup = '{ "userId": '+"\""+rayzmarker.userId+"\""+',"rayzReplyId": '+"\""+rayzmarker.rayzId+"\""+'}';
        jsonpup=jQuery.parseJSON(jsonpup);
        console.log(jsonpup);
    });
    placemarker.bindPopup("<div class='row'><div class='col-lg-9 col-md-9 col-xs-9'><p class='time_map'>"+time+"</p></div><div class='col-lg-3 col-md-3 col-xs-3'><i style='color:grey' class='fa fa-circle'></i></div></div>" +
    "<p class='reply'>" + rayzmarker.rayz_message + "</p>"+
        "<div class='restar-wrapper row'><p style='display:inline-block' class='starred'>" + rayzmarker.follow + " starred, </p><p style='display:inline-block'  class='rerayz'> " + rayzmarker.rerayz + " rerayz</p>"+
        "</div>");


    /*"<span class='time'>"+time+"</span><br>" +
     "<span>"+rayzmarker.rayz_message+"</span>" +
     "<br>"+
     "<span class='starred'>"+rayzmarker.follow+" starred </span>"+
     "<span class='rerayz'>,"+rayzmarker.rerayz+" rerayz</span><br>"+
     "<div class='markerdet'>"+
     "<a href='#'><i class='fa fa-star-o'></i></a>"+
     //"<a href='#'><i class='fa fa-star-o'></i></a>"+
     "</div>"*/

    /*$('.starred a').click(function(){
        if($(this).hasClass('unstarred')){
            $(this+'i').css('color','yellow');
            $(this).removeClass('unstarred');
            $(this).addClass('starred');
        }else if($(this).hasClass('starred')){
            $(this+'i').css('color','yellow');
            $(this).removeClass('unstarred');
            $(this).addClass('starred');

        }
    });*/
    markers.addLayer(placemarker);
    map.addLayer(markers);
}


function addUser(location){
    var markerStyle = L.AwesomeMarkers.icon({
        icon: 'user',
        prefix: 'glyphicon',
        markerColor: 'red'
    });
    var placemarker = L.marker([location.coords.latitude,location.coords.longitude],{icon:markerStyle});
    placemarker.addTo(map);
    map.panTo([location.coords.latitude,location.coords.longitude]);
    updateLocation(location.coords.latitude,location.coords.longitude);
    getPower();
    getLiveFeed();
    getRecentLiveFeed();
    getMyRayz();
}

function addRange(center,range){
    var str = ""+center;
    var coordinates = str.split(","); 
    var circle = L.circle([coordinates[0], coordinates[1]], range, {
        color: 'transparent',
        fillColor: '#ED3D23',
        fillOpacity: 0.5
    });
    circle.addTo(map);
}