<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="mobile-web-app-capable" content="yes">
        <meta name="apple-mobile-web-app-capable" content="yes">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        
        <title>Rayzit</title>
        
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>       
        <script src="_/libs/bootstrap/js/bootstrap.min.js"></script>
        <script src="_/libs/bootstrap/fileinput/js/fileinput.min.js"></script>
        <script src="_/libs/bootstrap/sweetalert/lib/sweet-alert.min.js"></script>
        <script src="_/js/jquery.slimscroll.min.js"></script>
        <script src="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.js"></script>
        <script src="_/libs/marker-awesome/dist/leaflet.awesome-markers.min.js"></script>
        <script src="_/libs/marker-cluster/dist/leaflet.markercluster.js"></script>
        <script src="_/libs/leaflet-context-menu/dist/leaflet.contextmenu.js"></script>
        <script src="_/libs/marker-list/dist/leaflet-list-markers.min.js"></script>
        <script src="_/libs/bootstrap/select/dist/js/bootstrap-select.min.js"></script>
        <script src="_/libs/add2homescreen/src/addtohomescreen.min.js"></script>
        <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.0/js/bootstrap-toggle.min.js"></script>
        
        <link rel="icon" type="image/ico" href="_/img/favicon.ico">
        <link href="_/libs/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <link href="_/libs/bootstrap/fileinput/css/fileinput.min.css" rel="stylesheet">
        <link href="_/libs/bootstrap/sweetalert/lib/sweet-alert.css" rel="stylesheet">
        <link href="_/libs/font-awesome/css/font-awesome.min.css" rel="stylesheet">
        <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
        <link rel="stylesheet" href="_/libs/marker-awesome/dist/leaflet.awesome-markers.css"/>
        <link rel="stylesheet" href="_/libs/marker-cluster/dist/MarkerCluster.css"/>
        <link rel="stylesheet" href="_/libs/marker-cluster/dist/MarkerCluster.Default.css"/>
        <link rel="stylesheet" href="_/libs/leaflet-context-menu/dist/leaflet.contextmenu.css"/>
        <link rel="stylesheet" href="_/libs/marker-list/dist/leaflet-list-markers.min.css"/>
        <link rel="stylesheet" href="_/libs/bootstrap/select/dist/css/bootstrap-select.min.css"/>
        <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.0/css/bootstrap-toggle.min.css" rel="stylesheet"> 
        <link href="_/libs/add2homescreen/style/addtohomescreen.css" rel="stylesheet"> 
        <link href="_/css/reset.css" rel="stylesheet">
        
        <link rel="apple-touch-icon" href="_/img/apple-icon-114x114-precomposed.png" />
        
        <script>
            var USER_SETTINGS;
            USER_SETTINGS=JSON.parse(localStorage.getItem('USER_SETTINGS'));
        </script>
    </head>
    <body>  
        <?php include 'components/navbar.php'?>
        <?php include 'components/forms/new_rayz_form.php'?>
        <?php include 'components/replies_list.php'?>
        <?php include 'components/star_rayz.php'?>
        <?php include 'components/my_rayz.php'?>
        <?php include 'components/settings.php'?>
        
        <div class="livefeed-widget">
            <span class="pull-right">
                <a href="#" id="show-livefeed"><i class="fa fa-globe fa-3x"></i></a>
            </span>
            <div class="livefeed-panel panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">Livefeed <a href="#" onclick="refresh();"><i class="fa fa-refresh fa-2x"></i></a></h3>
                </div>
                <div class="panel-body">                        
                    <ul class="nav nav-tabs">
                        <li role="presentation" class="active"><a href="#live" role="tab" data-toggle="tab">Live</a></li>
                        <li role="presentation"><a href="#recent" role="tab" data-toggle="tab">Recent</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="live">
                            <ul name="livefeed-list" class="list-group"></ul>
                        </div>
                        <div role="tabpanel" class="tab-pane fade" id="recent">
                            <ul name="recentfeed-list" class="list-group"></ul>
                        </div>
                    </div>                    
                </div>
            </div>
        </div>

        <div id="map_canvas"></div>
        <script src="_/js/decode_geohash.js"></script>
        <script src="_/js/global.js"></script>
        <script src="_/js/map.js"></script>
        <script src="_/js/rayzit.js"></script>
        <?php include "components/addTohome.php" ?>
        <script>
            
            $(function(){
                if (navigator.geolocation) {
                    navigator.geolocation.getCurrentPosition(addUser);
                } else {
                    console.log("Geolocation is not supported by this browser.");
                }
                
                $(document).on('click','.navbar-collapse.in',function(e) {
                    if( $(e.target).is('a') && $(e.target).attr('class') != 'dropdown-toggle' ) {
                        $(this).collapse('hide');
                    }
                });
                
                $('.tab-content').slimScroll({
                    height: '75vh'
                });
                
                
                $('#my-rayz-modal .modal-body').slimScroll({
                     height: '70vh'
                });

              
                $('#settings-modal .modal-body').slimScroll({
                     height: '70vh'
                });
                
                $("#show-livefeed").click(function(){
                    $(".livefeed-panel").slideToggle("fast");
                    $(this).toggleClass('pressed');
                });
                                
            });
        </script>
        
    </body>
</html>